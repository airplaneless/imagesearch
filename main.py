import os
import torch
import torch.optim as optim
import torch.nn.functional as F
import argparse
import numpy as np

from src.models import *
from src.utils import train, checkAutoencoder
from src.utils import SearchKDTree, SearchNN


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--model', type=str, help="""
        name of segmentaton net:
            convae_4, convae_8, convae_8_sp
    """)
    parser.add_argument('--epoch', type=int, help="""
        number of epochs
    """)
    args = parser.parse_args()
    # Init model
    if args.model == 'convae_4':
        net = ConvAutoencoder().cuda()
    elif args.model == 'convae_8':
        net = Autoencoder_8().cuda()
    elif args.model == 'convae_8_sp':
        net = ConvAutoencoderSubPixel().cuda()
    else:
        raise(ValueError('wrong --model'))
    # Init num epoch
    if not args.epoch or args.epoch < 1:
        raise(ValueError('wrong --epoch'))
    # Set model name
    model_name = str(net)
    print(torch.cuda.get_device_name(0))
    # Train model
    results = train(net=net,
                    criterion=torch.nn.MSELoss(),
                    optimizer=optim.Adam(net.parameters()),
                    fpath='workspace/checkpoints',
                    max_epoch=args.epoch)
    print(
        """
        ######################################################
        ####################SUMMARY###########################
        ######################################################        
        """
    )
    for k in results.keys():
        print('{} : {}'.format(k, str(results[k][-1])))
    # Set search method
    search_method = SearchNN
    err_mat = checkAutoencoder(net, method=search_method)
    if not os.path.exists('./workspace/models/{}'.format(model_name)):
        os.mkdir('./workspace/models/{}'.format(model_name))
    np.save('./workspace/models/{}/err.npy'.format(model_name), err_mat)
