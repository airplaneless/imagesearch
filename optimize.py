import os
import pickle
import torch
import torch.optim as optim
import torch.nn.functional as F
import numpy as np
import argparse
import time
import uuid
import warnings


from src.models import *
from src.utils import train, checkAutoencoder
from src.utils import SearchKDTree, SearchNN
from src.utils import CIFAR10_CLASSES

from sqlitedict import SqliteDict
from hyperopt import fmin, tpe, hp, Trials, STATUS_OK, STATUS_FAIL

os.environ["CUDA_VISIBLE_DEVICES"]="0,1"

global DATA_PATH, net, criterion


def f(x):
    optimizer = optim.Adam(
        net.parameters(),
        betas=(x['b1'], x['b2']),
        lr=x['lr'],
        weight_decay=x['weight_decay'],
    )
    results = train(net=net,
                    criterion=torch.nn.MSELoss(),
                    optimizer=optimizer,
                    fpath='workspace/checkpoints',
                    max_epoch=args.epoch,
                    name=model_name,
                    verbose=False)
    # Set search method
    search_method = SearchNN
    try:
        err_mat = checkAutoencoder(net, method=search_method, verbose=False)

    except ValueError:
        err_mat = np.zeros((len(CIFAR10_CLASSES), len(CIFAR10_CLASSES)), dtype=int)
        warnings.warn('Loss should not be NaN.')
        return {
                'loss': -np.sum(err_mat.diagonal()),
                'status': STATUS_FAIL
                }
    else:
        return {
            'loss': -np.sum(err_mat.diagonal()),
            'status': STATUS_OK
        }


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--model', type=str, help="""
        name of segmentaton net:
            convae_4, convae_8, convae_8_sp
    """)
    parser.add_argument('--epoch', type=int, help="""
        number of epochs
    """)
    parser.add_argument('--device', type=int, help="""
        number of gpu
    """)
    args = parser.parse_args()
    # Init model
    if args.device < 0:
        raise(ValueError('wrong --device: {}'.format(args.device)))

    print(torch.cuda.get_device_name(args.device))
    torch.cuda.set_device(args.device)

    if args.model == 'convae_4':
        net = ConvAutoencoder().cuda()
    elif args.model == 'convae_8':
        net = Autoencoder_8().cuda()
    elif args.model == 'convae_8_sp':
        net = ConvAutoencoderSubPixel().cuda()
    else:
        raise(ValueError('wrong --model'))
    # Init num epoch
    if not args.epoch or args.epoch < 1:
        raise(ValueError('wrong --epoch'))
    # Set model name
    model_name = str(net)+str(uuid.uuid4())[0:8]
    # Parameter optimization
    space = {
        'lr': hp.uniform('lr', 1e-6, 1e-2),
        'weight_decay': hp.uniform('weight_decay', 0, 1e-4),
        'b1': hp.uniform('b1', 0.2, 0.99),
        'b2': hp.uniform('b2', 0.2, 0.99),
    }

    trials = Trials()
    best = fmin(f, space, algo=tpe.suggest, max_evals=2, trials=trials, verbose=1)

    print("best: {}".format(best))
    db = SqliteDict('db/hyperopt_{}.db'.format(model_name), autocommit=True)
    db[model_name] = {'vals': trials.vals,
                 'loss': trials.losses()}