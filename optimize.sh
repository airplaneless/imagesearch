#!/bin/sh
#SBATCH -p gpu
#SBATCH -t 72:00:00
#SBATCH --gres=gpu:1
#SBATCH --mem=24576
python3.6 optimize.py --model convae_4 --epoch 2 --device 0 &
python3.6 optimize.py --model convae_4 --epoch 2 --device 1
