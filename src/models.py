import torch
import torch.nn as nn
import torch.nn.functional as F


class UpsamplingBilinear2d(nn.Module):
    def __init__(self, scale_factor):
        super(UpsamplingBilinear2d, self).__init__()
        self.scale = scale_factor

    def forward(self, x):
        x = F.interpolate(x, scale_factor=self.scale, mode='bilinear', align_corners=True)
        return x


class Reshape(torch.nn.Module):
    def __init__(self, n):
        super(Reshape, self).__init__()
        self.n = n

    def forward(self, x):
        x = x.view(-1, self.n)
        return x


class UnReshape(torch.nn.Module):
    def __init__(self, shape):
        super(UnReshape, self).__init__()
        self.shape = shape

    def forward(self, x):
        x = x.view(-1, self.shape[0], self.shape[1], self.shape[2])
        return x


class SubPixel(torch.nn.Module):

    def __init__(self, in_channels, out_channels, upscale_factor):

        super(SubPixel, self).__init__()
        self.conv = nn.Conv2d(in_channels, out_channels * upscale_factor ** 2, (3, 3), (1, 1), (1, 1))
        self.pixel_shuffle = nn.PixelShuffle(upscale_factor)

    def forward(self, x):
        return self.pixel_shuffle(self.conv(x))

# Writing our model
class Autoencoder_8(nn.Module):

    def __init__(self):

        super(Autoencoder_8, self).__init__()
        self.encoder = nn.Sequential(
            # 3x32x32 -> 64x28x28
            nn.Conv2d(3, 64, kernel_size=5),
            nn.ReLU(inplace=True),
            # 64x28x28 -> 64x14x14
            nn.MaxPool2d(2),
            # 64x14x14 -> 128x10x10
            nn.Conv2d(64, 128, kernel_size=5),
            nn.ReLU(inplace=True),
            # 128x10x10 -> 128x8x8
            nn.Conv2d(128, 128, kernel_size=3),
            nn.ReLU(inplace=True),
            # 128x6x6 -> 128x6x6
            nn.Conv2d(128, 128, kernel_size=3),
            nn.ReLU(inplace=True),
            # 128x6x6 -> 128x4x4
            nn.Conv2d(128, 128, kernel_size=3),
            nn.ReLU(inplace=True),
            # 128x4x4 -> 96x2x2
            nn.Conv2d(128, 96, kernel_size=3),
            nn.ReLU(inplace=True),
            # 96x2x2 -> 96x1x1
            nn.MaxPool2d(2),

        )

        self.decoder = nn.Sequential(
            UpsamplingBilinear2d(scale_factor=2),
            nn.ConvTranspose2d(96, 512, kernel_size=3),
            nn.ReLU(inplace=True),
            nn.ConvTranspose2d(512, 128, kernel_size=3),
            nn.ReLU(inplace=True),
            nn.ConvTranspose2d(128, 128, kernel_size=3),
            nn.ReLU(inplace=True),
            nn.ConvTranspose2d(128, 128, kernel_size=3),
            nn.ReLU(inplace=True),
            nn.ConvTranspose2d(128, 256, kernel_size=3),
            nn.ReLU(inplace=True),
            UpsamplingBilinear2d(scale_factor=2),
            nn.ConvTranspose2d(256, 12, kernel_size=5),
            nn.ReLU(inplace=True),
            nn.ConvTranspose2d(12, 3, kernel_size=5),
            nn.Sigmoid()
        )

    def __str__(self):
        return 'Autoencoder_8'

    def forward(self, x):
        x = self.encoder(x)
        x = self.decoder(x)
        return x


class ConvAutoencoder(nn.Module):
    def __init__(self):
        super(ConvAutoencoder, self).__init__()
        self.encoder = nn.Sequential(
            nn.Conv2d(3, 16, 3, padding=1),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(2),
            nn.Conv2d(16, 8, 3, padding=1),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(2),
            nn.Conv2d(8, 8, 3, padding=1),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(2)
        )

        self.decoder = nn.Sequential(
            UpsamplingBilinear2d(scale_factor=2),
            nn.Conv2d(8, 8, 3, padding=1),
            nn.ReLU(inplace=True),
            UpsamplingBilinear2d(scale_factor=2),
            nn.Conv2d(8, 16, 3, padding=1),
            nn.ReLU(inplace=True),
            UpsamplingBilinear2d(scale_factor=2),
            nn.Conv2d(16, 3, 3, padding=1),
            nn.Tanh()
        )

    def __str__(self):
        return 'ConvAutoencoder'

    def forward(self, x):
        x = self.encoder(x)
        x = self.decoder(x)
        return x


class ConvAutoencoderSubPixel(nn.Module):

    def __init__(self):

        super(ConvAutoencoderSubPixel, self).__init__()
        self.encoder = nn.Sequential(
            # 3x32x32 -> 64x28x28
            nn.Conv2d(3, 64, kernel_size=5),
            nn.ReLU(inplace=True),
            # 64x28x28 -> 64x14x14
            nn.MaxPool2d(2),
            # 64x14x14 -> 128x10x10
            nn.Conv2d(64, 128, kernel_size=5),
            nn.ReLU(inplace=True),
            # 128x10x10 -> 128x8x8
            nn.Conv2d(128, 128, kernel_size=3),
            nn.ReLU(inplace=True),
            # 128x6x6 -> 128x6x6
            nn.Conv2d(128, 128, kernel_size=3),
            nn.ReLU(inplace=True),
            # 128x6x6 -> 128x4x4
            nn.Conv2d(128, 128, kernel_size=3),
            nn.ReLU(inplace=True),
            # 128x4x4 -> 96x2x2
            nn.Conv2d(128, 96, kernel_size=3),
            nn.ReLU(inplace=True),
            # 96x2x2 -> 96x1x1
            nn.MaxPool2d(2),

        )

        self.decoder = nn.Sequential(
            SubPixel(96, 96, upscale_factor=2),
            nn.ConvTranspose2d(96, 512, kernel_size=3),
            nn.ReLU(inplace=True),
            nn.ConvTranspose2d(512, 128, kernel_size=3),
            nn.ConvTranspose2d(128, 128, kernel_size=3),
            nn.ReLU(inplace=True),
            nn.ConvTranspose2d(128, 128, kernel_size=3),
            nn.ReLU(inplace=True),
            nn.ConvTranspose2d(128, 256, kernel_size=3),
            nn.ReLU(inplace=True),
            SubPixel(256, 256, upscale_factor=2),
            nn.ConvTranspose2d(256, 12, kernel_size=5),
            nn.ReLU(inplace=True),
            SubPixel(12, 12, upscale_factor=1),
            nn.ConvTranspose2d(12, 3, kernel_size=5),
            nn.Sigmoid()
        )

    def __str__(self):
        return 'ConvAutoencoderSubPixel'

    def forward(self, x):
        x = self.encoder(x)
        x = self.decoder(x)
        return x



class DummyDeepAutocode:

    def __init__(self):
        pass
