import os
import torch
import torch.nn
import torch.utils.data
import torchvision
import torchvision.transforms as transforms

import numpy as np

from functools import reduce
from tqdm import tqdm
from sqlitedict import SqliteDict
from sklearn.neighbors import KDTree
from sklearn.neighbors import NearestNeighbors



DATASET_PATH = 'datasets'
CIFAR10_CLASSES = ('plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck')


class SearchMethod:

    def __init__(self, data: np.ndarray):
        self.data = data

    def find(self, x) -> int:
        return 0;

    def findKnearest(self, x) -> list:
        return [0];


class SearchNN(SearchMethod):

    def __init__(self, data: np.ndarray):
        super(SearchNN, self).__init__(data)
        self.nbrs = NearestNeighbors(n_neighbors=1, algorithm='auto').fit(self.data) # .fit(data)? , algorithm='auto'

    def find(self, x):
        dist, ind = self.nbrs.kneighbors([x])
        return ind[0][0]

    def findKnearest(self, x):
        dist, ind = self.nbrs.kneighbors([x])
        return ind[0]



class SearchKDTree(SearchMethod):

    def __init__(self, data: np.ndarray):
        super(SearchKDTree, self).__init__(data)
        self.tree = KDTree(self.data, leaf_size=2)

    def find(self, x):
        dist, ind = self.tree.query([x], k=1)
        return ind[0][0]

    def findKnearest(self, x, k=2):
        dist, ind = self.tree.query([x], k=k)
        return ind[0]



def train(net, criterion, optimizer,
          batch_size=8,
          max_epoch=5,
          fpath='.',
          name='untitled_model',
          verbose=True):

    # Dictionary with training curves
    result = {}
    # Loading and Transforming data CIFAR10
    transform = transforms.Compose([transforms.ToTensor()])
    trainset = torchvision.datasets.CIFAR10(root=DATASET_PATH, train=True, download=True, transform=transform)
    dataloader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, shuffle=False, num_workers=4)
    epochs = tqdm(range(max_epoch))
    # Arrays with loss values
    losses = np.zeros(max_epoch)
    # Training...
    for epoch in epochs:
        # Train on train dataset
        net.train()
        for batch in dataloader:
            imgs, _ = batch
            optimizer.zero_grad()
            outputs = net(imgs.cuda().float())
            loss = criterion(outputs.float(), imgs.cuda().float())
            loss.backward()
            optimizer.step()
            losses[epoch] += loss.item()
        # Make checkpoint
        torch.save(net, os.path.join(fpath, name + '_{}.pt'.format(epoch)))
        # Update sqlite3 database
        result['loss'] = losses / len(dataloader)
        # Update progress bar
        epochs.set_description('loss: {}'.format(losses[epoch] / len(dataloader)))

    return result


def checkAutoencoder(net, method=None, verbose=True):
    """
    Check image retrieval by classes
    :param net:
    :param method:
    :param verbose:
    :return: M, searcher, trainset, testset
    """
    # Load CIFAR10 train & test
    transform = transforms.Compose([transforms.ToTensor()])
    trainset = torchvision.datasets.CIFAR10(root=DATASET_PATH, train=True, download=True, transform=transform)
    testset = torchvision.datasets.CIFAR10(root=DATASET_PATH, train=False, download=True, transform=transform)
    # Getting code size
    test_img = torch.randn((1,3,32,32))
    code_sample = net.encoder(test_img.cuda())
    code_size = reduce(lambda x,y: x*y, list(code_sample.shape))
    # Coding all images from trainset
    encoded_trainset = np.zeros((len(trainset), code_size))
    for i in tqdm(range(len(trainset)), desc='Coding train images...', disable=not verbose):
        x, _ = trainset[i]
        x_encoded = net.encoder(x.reshape(1, 3, 32, 32).cuda()).flatten()

        encoded_trainset[i] = x_encoded.cpu().detach().numpy()
    # Init search method
    if SearchMethod is None:
        searcher = SearchMethod(encoded_trainset);
    else:
        searcher = method(encoded_trainset)
    # Coding all images from testset,
    # find closest in trainset,
    # evaluate error
    M = np.zeros((len(CIFAR10_CLASSES), len(CIFAR10_CLASSES)), dtype=int)
    for i, img in tqdm(enumerate(testset), total=len(testset), desc='Coding test images...', disable=not verbose):
        img, _ = img
        code = net.encoder(img.reshape(1, 3, 32, 32).cuda()).flatten().cpu().detach().numpy()
        found = searcher.find(code)
        pred = trainset[found][1]
        gt = testset[i][1]
        M[pred][gt] += 1
    return M, searcher


def plotErrorMat(mat: np.ndarray):
    import numpy as np
    import matplotlib
    import matplotlib.pyplot as plt
    from matplotlib import cm

    fig, ax = plt.subplots(figsize=(8, 8))
    plt.rcParams["axes.grid"] = False

    im = ax.imshow(mat, cmap=cm.jet)

    # We want to show all ticks...
    ax.set_xticks(np.arange(len(CIFAR10_CLASSES)))
    ax.set_yticks(np.arange(len(CIFAR10_CLASSES)))
    # ... and label them with the respective list entries
    ax.set_xticklabels(CIFAR10_CLASSES)
    ax.set_yticklabels(CIFAR10_CLASSES)

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    for i in range(len(CIFAR10_CLASSES)):
        for j in range(len(CIFAR10_CLASSES)):
            text = ax.text(j, i, mat[i, j],
                           ha="center", va="center", color="k")

    ax.set_title("Image classes matches")
    fig.tight_layout()
    plt.colorbar(im, orientation='vertical')
    ax.set_xlabel('true')
    ax.set_ylabel('pred')

    plt.show()